package io.chronosphere.java_apps;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;
 
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
 
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import io.prometheus.client.Summary;
import io.prometheus.client.exporter.common.TextFormat;
 
/**
 * This is a Java example of how to get started instrumenting a
 * Java class that could be a service or application. A simple
 * HTTP server is started to expose an endpoint on the host at
 * port 7777. 
 * 
 * The Prometheus Java instrumentation library is leveraged to 
 * demonstrate how to instrument the following four basic metrics
 * types:
 * 
 *  1. Counter
 *  2. Gauge
 *  3. Histogram
 *  4. Summary
 * 
 * Note: these metrics are started and provided random numbers to 
 *       instantiate the structures.
*/
public class JavaMetrics {
 
    private static final String QUERY_PARAM_SEPERATOR = "&";
    private static final String NAMESPACE_JAVA_APP = "java_app";
    private static final String UTF_8 = "UTF-8";
 
    private static class LocalByteArray extends ThreadLocal<ByteArrayOutputStream> {
        @Override
        protected ByteArrayOutputStream initialValue() {
            return new ByteArrayOutputStream(1 << 20);
        }
    }
 
    public static void main(String[] args) throws Exception {
 
        CollectorRegistry registry = CollectorRegistry.defaultRegistry;
 
        // Set up and start metrics endpoint.
        startMetricsPopulatorThread(registry);      
        startHttpServer(registry);
        System.out.println("");
        System.out.println("Java example metrics setup successful...");
        System.out.println("");

        // Insert code here for application or microservice.
        System.out.println("Java example service started...");
        System.out.println("");

    }
 
    private static void startMetricsPopulatorThread(CollectorRegistry registry) {
        Counter counter = counter(registry);
         
        Gauge gauge = gauge(registry);      
        Histogram histogram = histogram();
        Summary summary = summary(registry);
        
 
        Thread bgThread = new Thread(() -> {
            while (true) {
                try {
                    counter.inc(1);
                    gauge.set(rand(-5, 10));
                    histogram.observe(rand(0, 5));
                    summary.observe(rand(0, 5));
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        bgThread.start();
    }
 
    private static Summary summary(CollectorRegistry registry) {
        return Summary.build()
                .quantile(0.5, 0.05) // Add 50th percentile (= median) with 5% tolerated error
                .quantile(0.9, 0.01) // Add 90th percentile with 1% tolerated error
                .quantile(0.99, 0.001) // Add 99th percentile with 0.1% tolerated error               
                .namespace(NAMESPACE_JAVA_APP)
                .name("s")
                .help("is a summary metric (request size in bytes)")
                .register(registry);                            
    }
 
    private static Histogram histogram() {
        return Histogram.build()
                                .namespace(NAMESPACE_JAVA_APP)
                                .name("h")
                                .help("is a histogram metric")
                                .register();
    }
 
    private static Gauge gauge(CollectorRegistry registry) {
        return Gauge.build()
                        .namespace(NAMESPACE_JAVA_APP)
                        .name("g")
                        .help("is a gauge metric")
                        .register(registry);
    }
 
    private static Counter counter(CollectorRegistry registry) {
        return Counter.build()
                            .namespace(NAMESPACE_JAVA_APP)
                            .name("c")
                            .help("is a counter metric")
                            .register(registry);
    }
 
    private static double rand(double min, double max) {
        return min + (Math.random() * (max - min));
    }
     
    private static void startHttpServer(CollectorRegistry registry) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(7777), 0);
        HTTPMetricHandler mHandler = new HTTPMetricHandler(registry);
        addContext(server, mHandler);
        server.setExecutor(null); // creates a default executor
        server.start();
    }
 
    private static void addContext(HttpServer server, HTTPMetricHandler mHandler) {
        server.createContext("/", mHandler);
        server.createContext("/metrics", mHandler);
        server.createContext("/healthy", mHandler);
    }
 
    static class HTTPMetricHandler implements HttpHandler {
         
 
        private final static String HEALTHY_RESPONSE = "Exporter is Healthy.";
         
        private final CollectorRegistry registry;
        private final LocalByteArray response = new LocalByteArray();
 
        HTTPMetricHandler(CollectorRegistry registry) {
            this.registry = registry;
        }
 
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String query = exchange.getRequestURI().getRawQuery();
            String contextPath = exchange.getHttpContext().getPath();
 
            ByteArrayOutputStream outPutStream = outputStream();
 
            writeToStream(query, contextPath, outPutStream);            
            writeHeaders(exchange);
 
            gzipStream(exchange, outPutStream);
 
            exchange.close();
            System.out.println("Handled :" + contextPath );
        }
 
        private void gzipStream(HttpExchange exchange, ByteArrayOutputStream outPutStream) throws IOException {
            final GZIPOutputStream os = new GZIPOutputStream(exchange.getResponseBody());
 
            try {
                outPutStream.writeTo(os);
            } finally {
                os.close();
            }
        }
 
        private void writeHeaders(HttpExchange exchange) throws IOException {
            exchange.getResponseHeaders().set("Content-Type", TextFormat.CONTENT_TYPE_004);
            exchange.getResponseHeaders().set("Content-Encoding", "gzip");
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
        }
 
        private void writeToStream(String query, String contextPath, ByteArrayOutputStream outPutStream) throws IOException {
            OutputStreamWriter osw = new OutputStreamWriter(outPutStream, Charset.forName(UTF_8));
            if ("/-/healthy".equals(contextPath)) {
                osw.write(HEALTHY_RESPONSE);
            } else {
                TextFormat.write004(osw, registry.filteredMetricFamilySamples(parseQuery(query)));
            }
            osw.close();
        }
 
        private ByteArrayOutputStream outputStream() {
            ByteArrayOutputStream response = this.response.get();
            response.reset();
            return response;
        }
    }
 
    private static Set<String> parseQuery(String query) throws IOException {
        Set<String> names = new HashSet<String>();
        if (query != null) {
            String[] pairs = query.split(QUERY_PARAM_SEPERATOR);
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                if (idx != -1 && URLDecoder.decode(pair.substring(0, idx), UTF_8).equals("name[]")) {
                    names.add(URLDecoder.decode(pair.substring(idx + 1), UTF_8));
                }
            }
        }
        return names;
    }
}