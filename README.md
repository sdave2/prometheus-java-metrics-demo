# Simple Prometheus Metrics Instrumentation Java Example

This is a simple example of how to export a Counter with Prometheus.

Requirements:  Java 8+, Maven

## Build application

```bash
  mvn clean install

  java -jar target/java_metrics-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## Running application

View metrics:  <http://localhost:7777/metrics>

 ```text
# HELP java_app_s is a summary metric (request size in bytes)
# TYPE java_app_s summary
java_app_s{quantile="0.5",} 2.7207345918090224
java_app_s{quantile="0.9",} 4.544679544614086
java_app_s{quantile="0.99",} 4.972054319902216
java_app_s_count 3841.0
java_app_s_sum 9638.008422461535
# HELP java_app_h is a histogram metric
# TYPE java_app_h histogram
java_app_h_bucket{le="0.005",} 3.0
java_app_h_bucket{le="0.01",} 8.0
java_app_h_bucket{le="0.025",} 23.0
java_app_h_bucket{le="0.05",} 46.0
java_app_h_bucket{le="0.075",} 62.0
java_app_h_bucket{le="0.1",} 89.0
java_app_h_bucket{le="0.25",} 197.0
java_app_h_bucket{le="0.5",} 369.0
java_app_h_bucket{le="0.75",} 553.0
java_app_h_bucket{le="1.0",} 740.0
java_app_h_bucket{le="2.5",} 1928.0
java_app_h_bucket{le="5.0",} 3841.0
java_app_h_bucket{le="7.5",} 3841.0
java_app_h_bucket{le="10.0",} 3841.0
java_app_h_bucket{le="+Inf",} 3841.0
java_app_h_count 3841.0
java_app_h_sum 9643.100260720028
# HELP java_app_c is a counter metric
# TYPE java_app_c counter
java_app_c 3841.0
# HELP java_app_g is a gauge metric
# TYPE java_app_g gauge
java_app_g -4.579111595011371
 ```

## Prometheus configuration to scrape metrics

Add this to a Prometheus scrape configuration to enable metrics collection:

```yaml
scrape_configs:

# Scraping Java metrics.
  - job_name: "java_app"
    static_configs:
      - targets: ["localhost:7777"]
        labels:
          job: "java_app"
          env: "workshop-lab8"
```

## Implementation details for counter

This represents a cumulative metric that only increases over time, like the number of requests to an endpoint. Later you can query how fast the value is increasing using rate function. Some of the cases where you would use a counter:

- Number of requests processed

- Number of items that were inserted into a queue

- Total amount of data a system has processed

- Error Count

The counter used in this example is the `java_app_c` metric and it is only increasing each time:

```text
# HELP java_app_c is a counter metric
# TYPE java_app_c counter
java_app_c 19.0
```

The best way to query this in the console using PromQL is to look at the increase over time using the `rate()` function, such as:

```sql
rate(java_app_c[10m])
```

## Implementation details for gauge

Gauges are instant measurements of a value. Gauges represent a random value that can increase and decrease randomly such as the load of your system. Some cases for using gauges:

- In progress requests

- Number of items in a queue

- Free memory

- Total memory

The gauge used in this example is the `java_app_g` metric:

```text
# HELP java_app_g is a gauge metric
# TYPE java_app_g gauge
java_app_g 5.627685097265761
```

A gauge can both increase and decrease over time, so the best way to query is to use the `avg_over_time` function, such as:

```sql
avg_over_time(java_app_g[10m])
```

## Implementation details for histogram

A histogram samples observations (such as request durations or response sizes) and counts them in configurable buckets. It also provides a sum of all observed values which gives you an approximation. Rather than storing every duration for every request, Prometheus will make an approximation by storing the frequency of requests that fall into particular buckets. Some cases for using histograms:

- Response latency

- Request size

The histogram used in this example is the `java_app_h` metric and it exposes multiple time series during a scrape:

```text
# HELP java_app_h is a histogram metric
# TYPE java_app_h histogram
java_app_h_bucket{le="0.005",} 0.0
java_app_h_bucket{le="0.01",} 0.0
java_app_h_bucket{le="0.025",} 0.0
java_app_h_bucket{le="0.05",} 0.0
java_app_h_bucket{le="0.075",} 0.0
java_app_h_bucket{le="0.1",} 1.0
java_app_h_bucket{le="0.25",} 1.0
java_app_h_bucket{le="0.5",} 1.0
java_app_h_bucket{le="0.75",} 3.0
java_app_h_bucket{le="1.0",} 9.0
java_app_h_bucket{le="2.5",} 21.0
java_app_h_bucket{le="5.0",} 37.0
java_app_h_bucket{le="7.5",} 37.0
java_app_h_bucket{le="10.0",} 37.0
java_app_h_bucket{le="+Inf",} 37.0
java_app_h_count 37.0
java_app_h_sum 89.22511746552392
```

The best way to query it is with the `rate()` function and over time to see the duration of something over time:

```sql
rate(java_app_h_sum[5m]) / rate(java_app_h_count[5m])
```

For an advanced query see the use of the `histogram_quantile()` function:

```sql
histogram_quantile(0.95, sum(rate(java_app_h_bucket[5m])) by (le))
```

## Implementation details for summaries

Similar to a histogram, a summary samples observations. While it also provides a total count of observations and a sum
of all observed values, it calculates configurable quantiles over a sliding time window. Some cases for using summaries:

- Response latency

- Request size

- Request duration

The summary used in this example is the `java_app_s` metric that exposes multiple time series during a scrape (found in the
Prometheus metrics) for request size in bytes:

```text
# HELP java_app_s is a summary metric (request size in bytes)
# TYPE java_app_s summary
java_app_s{quantile="0.5",} 2.622521567961292
java_app_s{quantile="0.9",} 3.869333027818032
java_app_s{quantile="0.99",} 3.869333027818032
java_app_s_count 5.0
java_app_s_sum 13.944922531339028
```

An example of querying a summary:

```sql
rate(java_app_s_sum[5m]) / rate(java_app_s_count[5m])
```

## Releases

 v0.1 - Example Prometheus metrics based on Java simpleclient v0.6.
